var express = require('express');
var router = express.Router();
var fs = require('fs');

/* GET users listing. */
router.use('/:id', function(req, res, next) {
  fs.readFile('./steps/' + req.params.id + '.json', function (err, step) {
    if(err) throw err;
    var step = JSON.parse(step)
    var password = step.password.split(',')
    if(password.indexOf(req.query.password) >= 0){
      delete step.password
      res.json(step )
    } else{
      res.json({error: 'wrong password', errorCode: 1})
    }
  })
});

module.exports = router;
